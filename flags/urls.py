from django.urls import path

from . import views

app_name = 'flags'

urlpatterns = [
    path('', views.FlagTemplateView.as_view(), name='flags'),
    path('question/', views.get_question_set, name='question'),
    path('country/', views.get_matching_country, name='country'),
]
